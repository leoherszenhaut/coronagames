# README #

### What is this repository for? ###

This repo contains the necessary files to run "Virus Scan", 
a machine learning model to detect COVID-19 in chest X-ray images.

It uses Convolutional Auto-encoder (trained on Google Colab) to find 
anomalies in the original X-ray Image.

Please check the pdf of the presentation for some extra information.


### Who do I talk to? ###
Please feel free to contact us through LinkedIn

* [Ary Korenvais](https://www.linkedin.com/in/arykorenvais)

* [Benjamin Scholom](https://www.linkedin.com/in/ben-scholom-10812296/)

* [Carla Lasry](http://linkedin.com/in/carla-lasry-b11934113)

* [Leo Herszenhaut](https://www.linkedin.com/in/leo-herszenhaut-5b93b232/)