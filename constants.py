EXAMPLE_DATA_PATH = r'data_examples\IM-0115-0001.jpeg'

KERAS_MODEL_PATH = r'models\x_ray_autoencoder_keras_model.model'
PYTORCH_MODEL_PATH = r'models\x_ray_autoencoder_pytorch_model.pt'

COVID_THD = 0.02
