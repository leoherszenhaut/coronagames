import constants
import cv2
import numpy as np
import torch
from tensorflow.keras.models import load_model


# def predict_pt_model(file_path):
#
#     model = torch.load(constants.PYTORCH_MODEL_PATH).eval()
#     img = 'load from file'
#     recon_img = model(img)
#
#     return recon_img


def predict_ks_model(file_path):

    model = load_model(constants.KERAS_MODEL_PATH)

    orig_img = cv2.imread(file_path)
    img = np.expand_dims(np.expand_dims(cv2.resize(orig_img[:, :, 0], (512, 512)), 2), 0).astype('float32') / 255
    recon_img = model.predict(img)

    return img[0, :, :, 0], recon_img[0, :, :, 0]
