import anomaly_predictor
import constants
import cv2
import os
import time
from sklearn.metrics import mean_squared_error


def run_test(image_path, save_folder):
    """
    Mock function to be used for frontend development
    :return: tuple of current time in timestamp as str and a random np array as "image"
    """
    processed_time = int(1000 * time.time())
    saved_recon_path = os.path.join(save_folder, f"recon_{processed_time}.jpg")

    orig_img, recon_img = anomaly_predictor.predict_ks_model(image_path)

    anomaly_score = calculate_anomaly_score(orig_img, recon_img)
    # print(anomaly_score)

    if anomaly_score >= constants.COVID_THD:

        test_result = 'POSITIVE'

    else:

        test_result = 'NEGATIVE'

    cv2.imwrite(saved_recon_path, normalize_image(recon_img) * 255)

    return test_result, saved_recon_path


def mark_anomaly(original_image, reconstructed_image):

    sub_img = reconstructed_image - original_image
    sub_img[sub_img != 0] = 255
    cv2.imwrite("sub.jpg", sub_img)

    median = cv2.medianBlur(sub_img, 7)
    cv2.imwrite("median.jpg", median)

    edged = cv2.Canny(median, 30, 200)
    cv2.imwrite("edged.jpg", edged)

    contours, hierarchy = cv2.findContours(edged, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    anom = original_image.copy()
    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        anom = cv2.rectangle(anom, (x, y), (x + w, y + h), 0, 2)
        # anom = cv2.drawContours(anom, contours, -1, 0, 1)

    cv2.imwrite("anom.jpg", anom)

    return anom


def normalize_image(img):

    norm_img = img.copy()
    norm_img -= norm_img.min()
    norm_img /= norm_img.max()

    return norm_img


def calculate_anomaly_score(original_image, reconstructed_image):

    return mean_squared_error(original_image, reconstructed_image)


def main():

    test_result, processed_image_path = run_test(constants.EXAMPLE_DATA_PATH, "images\\")

    # orig_img = cv2.imread(constants.EXAMPLE_DATA_PATH, cv2.IMREAD_GRAYSCALE)
    # anom_img = cv2.imread(r'data_examples\IM-0115-0001-A.jpeg', cv2.IMREAD_GRAYSCALE)
    #
    # proc_img = mark_anomaly(orig_img, anom_img)


if __name__ == '__main__':
    main()
